package postresql

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"snippet3/pkg/models"
	"time"
)

type SnippetModel struct {
	DataBase *pgxpool.Pool
}

func (m *SnippetModel) Insert(title, content string, expires int) (int, error) {
	stmt := `INSERT INTO snippets (title, content, created, expires)
VALUES($1, $2, $3, $4) RETURNING id`
	var id int64
	var err = m.DataBase.QueryRow(context.Background(), stmt, title, content, time.Now(), time.Now().AddDate(0, 0, expires)).Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}
func (m *SnippetModel) Get(id int) (*models.Snippet, error) {
	stmt := `SELECT id, title, content, created, expires FROM snippets
	WHERE expires > current_timestamp 
	AND id = $1`
	result := m.DataBase.QueryRow(context.Background(), stmt, id)
	snippet := &models.Snippet{}
	err := result.Scan(&snippet.ID, &snippet.Title, &snippet.Content, &snippet.Created, &snippet.Expires)
	if err != nil {
		return nil, models.ErrNoRecord
	}
	return snippet, nil
}
func (m *SnippetModel) Latest() ([]*models.Snippet, error) {
	stmt := `SELECT id, title, content, created, expires FROM snippets WHERE expires > current_timestamp ORDER BY created DESC LIMIT 10`
	result, err := m.DataBase.Query(context.Background(), stmt)
	if err != nil {
		return nil, err
	}
	defer result.Close()
	snippets := []*models.Snippet{}
	for result.Next() {
		snippet := &models.Snippet{}
		err = result.Scan(&snippet.ID, &snippet.Title, &snippet.Content, &snippet.Created, &snippet.Expires)
		if err != nil {
			return nil, err
		}
		snippets = append(snippets, snippet)
	}
	return snippets, nil
}
