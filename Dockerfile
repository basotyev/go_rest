FROM golang

RUN mkdir -p /usr/src/snippet
WORKDIR /usr/src/snippet

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

EXPOSE 4000

RUN go build -o snippet /usr/src/snippet/cmd/web/
ENTRYPOINT /usr/src/snippet/snippet